# encoder

![](https://img.shields.io/badge/written%20in-VB6-blue)

A batch video re-encode utility.

This is a tool for batch media encoding (in the style of BENCOS, RealAnime and MiniCoder), though it works equally well for single encodes. It uses x264, neroAAC and mkvmerge.

The GUI is written in native Win32/VB6 (no .NET dependency), quite logical and easy to use, and tested to run on both XP and Windows 7. It retains subtitles and chapters in source mkv files. Settings are stored to .ini in the same directory, and it includes a simple bitrate calculator as well as a short guide in the GUI.

Configuration is minimal, but it includes audio/video source input filters (including Import), custom avisynth, choice of ratecontrol for both x264 and neroAacEnc, bitrate/crf, and x264's preset and tune. 

For more information, see the original doom9 thread: http://forum.doom9.org/showthread.php?t=151677


## Download

- [⬇️ encoder_r54.rar](dist-archive/encoder_r54.rar) *(38.12 KiB)*
